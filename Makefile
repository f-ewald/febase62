TEST_CMD = pytest
CHECKSTYLE_CMD = python -m flake8

all: test coverage checkstyle 

test:
	$(TEST_CMD) --cov=febase62 --hypothesis-show-statistics

checkstyle:
	$(CHECKSTYLE_CMD) febase62/*.py

coverage:
	codecov

clean:
	rm -f febase62/*.pyc
	rm -rf febase62/__pycache__
