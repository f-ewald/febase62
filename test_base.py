from febase62.base62 import Base62
from hypothesis import given
from hypothesis.strategies import integers
import pytest


class TestMethods:
    def test_encode(self):
        b = Base62()
        assert 'base62' == b.encode(34441886726)

    def test_encode_zero(self):
        b = Base62()
        assert '0' == b.encode(0)

    def test_decode(self):
        b = Base62()
        assert 34441886726 == b.decode('base62')

    @given(integers(min_value=0))
    def test_random_positive(self, number):
        b = Base62()
        assert b.decode(b.encode(number)) == number

    @given(integers(max_value=-1))
    def test_random_negative(self, number):
        b = Base62()
        with pytest.raises(ValueError):
            assert b.decode(b.encode(number)) == number
